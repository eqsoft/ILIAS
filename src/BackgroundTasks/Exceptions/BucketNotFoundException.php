<?php

use ILIAS\BackgroundTasks\Exceptions\Exception;

class BucketNotFoundException extends Exception {}