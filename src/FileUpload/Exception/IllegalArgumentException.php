<?php

namespace ILIAS\FileUpload\Exception;

/**
 * Class IllegalArgumentException
 *
 * Indicates illegal arguments given to a method or constructor.
 *
 * @author  Nicolas Schäfli <ns@studer-raimann.ch>
 * @since   5.3
 * @version 1.0
 *
 * @public
 */
class IllegalArgumentException extends \Exception {

}