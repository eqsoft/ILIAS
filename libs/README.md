# Dependency Management in ILIAS

The are currently three type of external libraries:
- PHP dependencies installed using composer in /libs/composer, see [composer-readme](libs/composer/README.md).
- PHP dependencies installad manually, located in some /Services/\*/libs and /Modules/\*/libs
- JS- and client-side Frameworks, currently also located in /Services\/* and /Modules\/* 